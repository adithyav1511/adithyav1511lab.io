---
layout: page
title: About Me
permalink: /about/
date: 2020-05-29
---

## Who wrote these wonderful blogs you ask?

**I am Adithya, a deeplearning and space enthusiast, who loves tweaking electronics.**

Don't know how they go well together?

Neither did I, but apparently I figured it out!

- I build nanosats (at least the electronics for it) with a wonderful team at college
- I get to work with people from around the world in analyzing satellite telemetry
  (check us out at [Polaris](https://www.polarisml.space))!

## Want to connect with me?

You can find me on:

- GitLab - [adithyav1511](https://gitlab.com/adithyav1511)
- GitHub - [MajorCarrot](https://github.com/MajorCarrot/)
- Matrix - [Major LightYear](https://riot.im/app/#/user/@majrocarrot:matrix.org?action=chat)
- LinkedIn - [Adithya Venkateswaran](https://www.linkedin.com/in/adithya-venkateswaran-770430166/)
- Facebook - [adithyav1511](https://www.facebook.com/adithyav1511/)
- Twitter - [@sillywolfofbog](https://twitter.com/sillywolfofbog/)

![Me]({{site.baseurl}}/img/about/about_me.png "His greatness")
