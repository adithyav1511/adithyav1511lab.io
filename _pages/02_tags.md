---
title: Tag Portal
permalink: /tags/
layout: page
excerpt: Articles sorted by tags.
date: 2020-05-31
---

{% for tag in site.tags %} {% capture name %}{{ tag | first }}{% endcapture %}

<h4 class="post-header" id="{{ name | downcase | slugify }}">
  {{ name | upcase }}
</h4>
{% for post in site.tags[name] %}
<article class="posts">
  <span class="posts-date">{{ post.date | date: "%b %d" }}: <b><a href="{{ post.url }}">{{ post.title | escape }}</a></b>
  </span>
</article>
{% endfor %} {% endfor %}
