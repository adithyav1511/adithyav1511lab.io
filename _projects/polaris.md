---
layout: project
title: "Polaris ML"
image: "{{site.baseurl}}/img/projects/Polaris/polaris.png"
date: 2020-05-30
---

### Machine Learning applied to satellites.

Satellite health is probably something which gives satellite operators sleepless nights. At Polaris, we provide you with open-source tools to never have to worry about your satellite health again.

Our aim is to utilise the full capacity of the SatNOGs network, and space weather data to create a powerful tool which can alert satellite operators about any problems with their satellite. All of this thanks to machine learning!
