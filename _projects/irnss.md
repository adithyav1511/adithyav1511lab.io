---
layout: project
title: "IRNSS"
image: "{{site.baseurl}}/img/projects/IRNSS/irnss.jpg"
date: 2020-05-30
---

### Receiver-Device Interface

The one thing I feel is most important in the world is access to free,
open-source information.

I created a CLI program which can interface with a complex closed source
receiver module, for IRNSS built by ACCORD systems, and improved performance
by manifold compared to the proprietary product.
