---
layout: post
title: "Community Bonding"
date: 2020-06-20 12:11:00 +0530
tags: [GSoC, space]
description: The Community Bonding Period
image: "{{site.baseurl}}/img/posts/2020-06-20-one-month-of-gsoc/bonding_agent.png"
---

Time is flying (rocket puns XD). It has been a month (two actually) already and
there is a lot of stuff I have to talk about.

## Betsi

Nope that is not my new girlfriend. But I am passionate about it nonetheless.
So what is it exactly?

Imagine the tail of a cow, an elephant's trunk in a mouse's body. Now keep
imagining and ask the others to imagine. Do you feel odd (and kind of like a
fool)? So does betsi!

betsi is nothing but an AI which helps find odd behaviour in data. Period.
It is shorthand for Behaviour Extraction using Time Series Investigation, but
we'll stick to betsi ; -)

I had to read a paper.

#### A research paper

### **A RESEARCH PAPER**

Can you believe that! Me reading and implementing a research paper. Who knew.

![It wasn't all that bad though, to be fair]({{site.baseurl}}/img/posts/2020-06-20-one-month-of-gsoc/woah.gif "Research and me")

So I spent day and night on betsi, showering all my smiles and tears, rejoicing
every successful pipeline, spending sleepless nights trying to perfect it.
And boy am I proud.

So here I present the grand structure.

|=> Input

|=> Processed input (zero mean and unit deviation, column vectors)

|=> More processing (autoencoder, encoder-decoder)

|=> Even more processing (distance measurement)

|=> Event detection (finally)

(Read more about it [here](https://gitlab.com/librespacefoundation/polaris/betsi))

I learnt how to setup a python project from scratch, write .gitlab-ci.yml files, 
write testing code, develop usable API*.

(\* You be the judge of that : P)

## What else?

Databases, databases, databases. I even made a song about it.

![Don't worry, I tried JSON as well]({{site.baseurl}}/img/posts/2020-06-20-one-month-of-gsoc/to-die-or-not-to.gif "To database or not to, that is the question")

``` text
Start my day with MariaDB,
Then try out MongoDB.
If bored slap myself
and play with InfluxDB.

Not night yet,
bring out ArangoDB.
Oh MySQL,
and PostgreSQL.
Won't you let me sleep
```

I am no Shakespeare, but that legit sounds great.

## And?

1. This blog of course. This is a product created by me during GSoC, for which I
sat and learnt CSS, SASS, liquid, HTML5... It might not sound like much, but
trust me, the dark circles in my eyes stand evidence of the effort I put in!
2. Polaris Friday Meetings (I love this)
3. More Polaris contrib (GPU param setting), dask tests...

## TL; DR

Learnt a lot, read the horrible song above, before-after pictures are the worst.
