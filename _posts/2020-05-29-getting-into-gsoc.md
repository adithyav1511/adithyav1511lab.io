---
layout: post
title: "Getting into GSoC!"
date: 2020-05-29 21:00:00 +0530
tags: [GSoC, space]
description: My journey to GSoC
image: /img/posts/2020-05-29-getting-into-gsoc/gsoc_logo.png
---

I know, I know that I am quite late at writing this blog (by almost a month),
but what can I say, I wanted to write a lot, but had too little time!

This post is just for me to remember, and for you to learn, on what I
went through to get GSoC and how I should thank Covid-19. Don't follow?
Read on :P

## Selecting Libre Space Foundation

For those of you who don't know, I am a big time space enthusiast. So
while searching for GSoC projects, I searched for, wait for it, space
science. That pretty much narrowed it down to Libre Space Foundation.

My takeaway: always choose your passion, it will end up working for you
eventually!

### What about the project?

Even though I had chosen the organization, the project was yet to be
chosen. I initially worked on the HRPT decoder, breaking my head over
setting everything up and all that boring stuff. Fast forward a couple
of weeks, I had a vague understanding of how gnu-radio works, and severe
sleep deprivation.

That's when my knight in shining armor came: **Polaris**.

![My knight in shining armor Polaris (╯✧ ∇ ✧)╯]({{site.baseurl}}/img/posts/2020-05-29-getting-into-gsoc/knight_in_shining_armor.png "Sir Polaris")

### What's so special about Polaris anyway?

In two words **the team**. What I love about contributing to Polaris is
the team I am contributing with. When I first saw the project listed in
the GSoC projects page for Libre Space, I knew I had a lot to learn, but
I also knew that it was right up my alley. I was half-anxious, half-excited
to join the Polaris and when I ended up garnering enough courage to introduce
myself, I received the warmest welcome; I felt at home.

And that is what keeps me going, the knowledge that I am working with a group
of highly talented people who are passionate about what they do, who want
the best for the project and who would do anything to help! They even
sent me a welcome poster on my first (cough, cough third ;-P) contribution
to Polaris.

![Thanks Hugh (@SaintAardvark)]({{site.baseurl}}/img/posts/2020-05-29-getting-into-gsoc/welcome_to_polaris.png "(｡♥‿♥｡)")

I could feel the same passion growing in me, I wanted to call myself a
Polaris member, to contribute back to the community who helped me, so I
did. This helped me grow as a person and a team member! It was no longer
my work, it was our work (communism).

My takeaway: As the greatest master this universe has seen said
"Pass on what you have learned." and "No! Try not! Do or do not,
there is no try." Can't frame it any better than that! XD

## Making use of time

You might or might not have been searching for the use of Covid-19
in this post till now. Either way here it is!

Thanks to that-which-shall-not-be-named (I know I have mentioned its name)
I got two months of alone time at home, time which I used wisely
~~watching TV series and movies (I did this but you should not know)~~
understanding the polaris pipeline. I experimented with different modules
of Polaris, submitted MRs for issues from the issues page and did what I
do best (usually in exams), solve stuff I don't know completely with
the help of others.

I loved working on Polaris, it kept me up day and night. It was like
a new unexplored puzzle, a 5 by 5 Rubik's cube perhaps. I learnt and
learnt and learnt, soaking everything up like a dry sponge. I am sure
I wouldn't have been able to spend so much time had it not been for the
unexpected holidays from Covid-19 (Every cloud has a silver lining eh).
By the end of March, I had started helping other newcomers out with their
MRs and environments and ...!

My takeaway: "It is our choices... that show what we truly are, far more than our abilities." -- Dumbledore

## Writing the proposal

I spent about two weeks writing my proposal, which you can find [here](https://docs.google.com/document/d/1GVbmv9YMhXaJt9hbE953zJ0iL7Dt21HpBrtxRQMbKvk/edit#). I am not sure what
aspects of the proposal (if at all any) helped my case, but my proposal
reflected who I am, what I love. I believe in keeping everything organised,
scheduled and I tried my best to show that in my proposal.

My takeaway: Let your work reflect your attitude.

## TL;DR

- Love what you do
- Do what you love
- Stay organized
- Manage your time

And may the force be with you!

![Seriously]({{site.baseurl}}/img/posts/2020-05-29-getting-into-gsoc/may_the_force_be_with_you.jpg "Control, control, you must learn control!")
