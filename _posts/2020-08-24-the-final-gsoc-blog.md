---
layout: post
title: "The Final GSoC blog"
date: 2020-08-24
tags: [GSoC, space]
description:
---

**Statutory warning**: This blog is for documentation purposes. It will
mostly be filled with facts and little story. If you want a more free-flowing
experience, please read my previous blog posts.

## The projects

### Polaris

A CLI based satellite telemetry analysis tool which uses XGBoost inside and
3d-force-graph for the output!

Lets break that down.

Polaris is an awesome command line tool which aims at making the life of
satellite operators easier by manifold. The tool has 4 parts.

1. polaris fetch: It fetches telemetry from the SatNOGs network, space weather
from SWPC (NOAA)
2. polaris learn: A machine learning (XGBoost) based module that analyses the
relationship between all data "fetched" and gives a json graph file as output.
3. polaris viz: A 3d graph based visualization module, which gives an
intuitive graph representation of data as output.
4. polaris anomaly (WIP): An autoencoder (betsi) based tool that detects
anomalies in telemetry data and warns satellite operators.

This was the main project I was working on. All the work I did are used as
modules here.

I know that pictures speak greater than a thousand words. So here are some
for you to drool over :P

![CubeBel-1]({{site.baseurl}}/img/posts/2020-08-24-the-final-gsoc-blog/CubeBel1.png "CubeBel-1")

![Quetzal-1]({{site.baseurl}}/img/posts/2020-08-24-the-final-gsoc-blog/quetzal1.png "Quetzal-1")

## The modules

### Vinvelivaanilai (space weather)

Vinvelivaanilai translates to space weather in Tamil. It is a python module
which uses FTP services to fetch space weather data from SWPC/NOAA's
servers and stores it locally or in influxdb based docker-containers.

It also contains functions to parse TLEs and OMMs (any GP data) and propagate
the orbit to find the position and velocity of the satellite at any time.
All red nodes in the Quetzal-1 graph (2nd graph above) belong to
vinvelivaanilai.

### Betsi:

Betsi is shorthand for "Behaviour Extraction for Time-Series Investigation". It
uses the power of deep learning :trademark: to detect anomalies in the telemetry
data. An anomaly can range from a simple orientation change to a mega scale
explosion which wiped out all of humanity. If it happened, betsi detected it*.

\* You can always change the sensitivity though :P

One more graph? The black dotted lines are the breakpoints. We are working
on finding a better way to represent 200 params we use for anomaly
detection though. So any opinions/views are welcome at our
[matrix/element chatroom](https://app.element.io/#/room/#polaris:matrix.org)

![They are not real spikes]({{site.baseurl}}/img/posts/2020-08-24-the-final-gsoc-blog/anomaly_detect.png "Anomalies detected by betsi")

### Wrapping it all up

A high level representation of everything in Polaris:

![It's all coming together]({{site.baseurl}}/img/posts/2020-08-24-the-final-gsoc-blog/polaris_arch.png "A complete polaris workflow")

## The work

To see what my original plan was, you can checkout my GSoC proposal
[here](https://docs.google.com/document/d/1GVbmv9YMhXaJt9hbE953zJ0iL7Dt21HpBrtxRQMbKvk/).

**What I ended up learning was much much more.**

1. I learnt to read, comprehend and implement a full blown research paper as a
part of a whole new project betsi. This will be used inside Polaris to detect
anomalies in satellite telemetry!
2. I interfaced with an ftp server and created a stable API to fetch space
weather data.
3. I went through a whole bunch of DBMS systems, tried them all out and chose
what would be used for storing the space weather data. This was a very unique
learning experience because I had to choose something future proof, but feature
rich, tailored to suit our needs (time series data) (and as my mentors half
joked, my whole career was at stake with that choice).
4. I fully familiarised with and modified an existing API (Polaris'), finding
scalable ways to add new space weather data. The infrastructure can combine
telemetry and space weather frames allowing Polaris to give more accurate
and relavent links.
5. I learnt to use liquid, CSS and javascript to create a whole blog (the one
you are reading on) and contributed to improving the user experience while
viewing the graph (WebGL and D3js)!
6. I am adding (MR is already submitted) a way to skip the normalization step
(normalization to SI units) which will help add support to all satellite
telemetry which can be decoded. I am currently analyzing the difference in
results produced by normalized and non-normalized frames. The Bobcat-1 team
tried it on an open loop testbench and we were able to validate some results
(all the telemetry was completely independent of space weather).
7. I guided new users on how to set up Polaris and helped them troubleshoot
their installations.

You can find a list of commits with their corresponsing merge requests in
[this google sheet](https://docs.google.com/spreadsheets/d/1OUVnI2uxR2T98s6PAyqsyOlI9XK6BYwMQLjQ_1ck76c)

## Work left to do

1. Finish betsi integration in Polaris. A path has been charted in
[an issue](https://gitlab.com/librespacefoundation/polaris/polaris/-/issues/110)
2. Find an innovative way to represent the betsi data (in a graph probably)
3. Add TLE/OMM propagation once SatNOGs opens up their database.
4. Add features to the viz module requested by satellite operators (viewing a
list of nodes, hiding unused nodes, etc.)

## A message to my mentors

All of this was possible because of your support. You not only helped me in my
work but also helped me grow as an individual. I learnt so much
more than just programming. I learnt to respect and enjoy the open-source
culture, make my own decisions, put my point across and defend it. I learnt to
be self-sufficient but also approach you when I need it (you were always
there to guide me). If any of you are reading this, please know that you have
helped me realize the potential I carry in me and I will forever be indebted to
you for that!

## Is this the end of my journey with Polaris

There is a hindi movie dialogue which goes like *picture abhi baaki hai mere dost*
(the film's not over yet, my friend). I plan to stay with LSF and the Polaris
team for a long, long time. Hopefully, I will make it to the list of core-contributors
sometime in the future ;-)
