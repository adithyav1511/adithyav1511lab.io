---
layout: post
title: "The First Evaluation"
date: 2020-07-02 00:41:00 +0530
tags: [GSoC, space]
description: The Community Bonding Period
---

There are many things which send chills down the spine. Death, ghosts, that
bully who is ready to give you a wedgie and choosing the right database.

And I just faced one of them (nope not the wedgie, maybe death (so I will send
shivers down your spine?)). No prizes for guessing it right, "choosing the
database" is the correct answer (more about the database at the end).

Jokes apart, it has been a great month. First evaluations are over and I
received the most heartwarming message from my mentor, appreciating my attitude
and the effort I put into my work. If you are reading this Red, I want you to
know that I couldn't have asked for a better mentor than you. I am not only
growing as a programmer but also as a person and hope that one day, I would
be able to guide and appreciate students just like you do with me!

![Yes you have the biggest heart!]({{site.baseurl}}/img/posts/2020-07-02-two-months-of-gsoc/big-heart-little-minds.jpg "Can't agree with this more"){: height="350px" }

So emotional stuff aside, what has been cooking in the polaris kitchen
you ask? Well vinvelivaanilai (Ah another weird/beautiful sounding project
name). It is a module to store and retrieve space-weather data and propagate
TLEs.

## The analogy

That was a whole bunch of jargons but it will all become clear shortly.
Say it is a rainy day and you have hung your clothes outside, what will happen?
It will get wet of course. But what if it was raining somewhere else. Will that
have any effect on your clothes? No.

Space-weather is like rain (it is more complex and is influenced by the sun,
distant stars/supernovae, earth's magnetic feild... but it has a similar
effect). Just like rain wets the clothes, space-weather has its effect on
anything in space (so naturally satellites are affected).

![Thank god for goldilocks]({{site.baseurl}}/img/posts/2020-07-02-two-months-of-gsoc/space_weather.jpg "Cool right"){: height="350px" }

The position of the satellite matters as well, if it is not in front of the sun,
it will not be affected by the sun (just like the clothes are not affected by
rains somewhere else). TLEs (Two Line Elements) help us find the position of
the satellite (coordinates of the satellite) for the same.

## Why is it important?

The satellite health depends on the space-weather and since polaris aims at
predicting satellite health, vinvelivaanilai becomes important (and also
because I am doing it. Are you questioning my intensions?)

![Crushed neck]({{site.baseurl}}/img/posts/2020-07-02-two-months-of-gsoc/disturbing_it_is.jpg "Believe in the force"){: height="350px" }

## What else?

Well, polaris is getting quite popular. Red was invited to three meetups in a
week (GitLab, H2O.ai and SGAC) which was great publicity for the project.

Ah, the database (almost forgot about it); the winner of the first and hopefully
last vinvelivaanilai database challange is, drum rolls please, influxdb 2.0.
The competition was great this year. The [songs written]({{ site.baseurl }}/one-month-of-gsoc)
about it will go down in historya and it was probably the toughest decision
I had to take in my quest to become a Jedi (I mean a polarite?).
